<?php

namespace Tests\Unit\Readers;

use App\Readers\JsonReader;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class JsonReaderTest extends ReaderTestCase
{
    public function testRead()
    {
        $file = vfsStream::newFile('questions.json')
            ->withContent('[{"foo":"foo1","bar":"bar1","baz":[1,2,3]},{"foo":"foo2","bar":"bar2","baz":[4,5,6]},{"foo":"foo3","bar":"bar3","baz":[7,8,9]}]')
            ->at($this->root);

        $this->assertEquals(
            [
                ['foo' => 'foo1', 'bar' => 'bar1', 'baz' => [1, 2, 3]],
                ['foo' => 'foo2', 'bar' => 'bar2', 'baz' => [4, 5, 6]],
                ['foo' => 'foo3', 'bar' => 'bar3', 'baz' => [7, 8, 9]],
            ],
            (new JsonReader($file->url()))->read()
        );
    }
}
