<?php

namespace Tests\Unit\Readers;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

abstract class ReaderTestCase extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    protected $root;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->root = vfsStream::setup('resources');
    }
}
