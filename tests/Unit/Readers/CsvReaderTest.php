<?php

namespace Tests\Unit\Readers;

use App\Readers\CsvReader;
use org\bovigo\vfs\vfsStream;

class CsvReaderTest extends ReaderTestCase
{
    public function testRead()
    {
        $file = vfsStream::newFile('questions.csv')
            ->withContent(
                <<< EOT
foo,bar,baz
1,2,3
4,5,6
7,8,9

EOT
            )
            ->at($this->root);

        $this->assertEquals(
            [
                ['foo' => 1, 'bar' => 2, 'baz' => 3],
                ['foo' => 4, 'bar' => 5, 'baz' => 6],
                ['foo' => 7, 'bar' => 8, 'baz' => 9],
            ],
            (new CsvReader($file->url()))->read()
        );
    }
}
