<?php

namespace Tests\Unit\Writers;

use App\Writers\JsonWriter;
use org\bovigo\vfs\vfsStream;

class JsonWriterTest extends WriterTestCase
{
    public function testWrite()
    {
        $file = vfsStream::newFile('questions.json')
            ->withContent('[{"foo":"foo1","bar":"bar1","baz":[1,2,3]},{"foo":"foo2","bar":"bar2","baz":[4,5,6]}]')
            ->at($this->root);

        $this->assertTrue(
            (new JsonWriter($file->url()))->write([
                'foo' => 'foo3',
                'bar' => 'bar3',
                'baz' => [7, 8, 9],
            ])
        );

        $this->assertSame(
            '[{"foo":"foo1","bar":"bar1","baz":[1,2,3]},{"foo":"foo2","bar":"bar2","baz":[4,5,6]},{"foo":"foo3","bar":"bar3","baz":[7,8,9]}]',
            $file->getContent()
        );
    }
}
