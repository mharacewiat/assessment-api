<?php

namespace Tests\Unit\Writers;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

abstract class WriterTestCase extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    protected $root;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->root = vfsStream::setup('resources');
    }
}
