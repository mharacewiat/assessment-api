<?php

namespace Tests\Unit\Writers;

use App\Writers\CsvWriter;
use org\bovigo\vfs\vfsStream;

class CsvWriterTest extends WriterTestCase
{
    public function testWrite()
    {
        $file = vfsStream::newFile('questions.csv')
            ->withContent(<<< EOT
foo,bar,baz
1,2,3
4,5,6

EOT
            )
            ->at($this->root);

        $this->assertTrue(
            (new CsvWriter($file->url()))->write([
                'foo' => 7,
                'bar' => 8,
                'baz' => 9,
            ])
        );

        $this->assertSame(
            <<< EOT
foo,bar,baz
1,2,3
4,5,6
7,8,9

EOT
            ,
            $file->getContent());
    }
}
