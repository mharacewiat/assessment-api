<?php

namespace Tests\Unit\Readers;

use App\Services\GoogleTranslateService;
use PHPUnit\Framework\TestCase;
use Stichoza\GoogleTranslate\GoogleTranslate as Client;

/**
 * GoogleTranslateServiceTest class.
 *
 * @package Tests\Unit\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class GoogleTranslateServiceTest extends TestCase
{
    public function testTranslate()
    {
        $client = $this->createPartialMock(Client::class, ['translate']);

        $client
            ->expects($this->once())
            ->method('translate')
            ->with($what = 'foo')
            ->willReturn($translation = 'oof');

        $this->assertSame(
            $translation,
            (new GoogleTranslateService($client))->translate($what, null)
        );
    }

    public function testTranslateWithLanguage()
    {
        $client = $this->createPartialMock(Client::class, ['setTarget', 'translate']);

        $client
            ->expects($this->once())
            ->method('setTarget')
            ->with($language = 'bar')
            ->willReturnSelf();

        $client
            ->expects($this->once())
            ->method('translate')
            ->with($what = 'foo')
            ->willReturn($translation = 'oof');

        $this->assertSame(
            $translation,
            (new GoogleTranslateService($client))->translate($what, $language)
        );
    }
}
