<?php

return [

    'path'     => env('QUESTIONS_PATH', database_path('resources/questions.json')),
    'language' => env('QUESTIONS_LANGUAGE', 'en'),

];
