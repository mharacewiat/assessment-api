Assessment API
=

# Table of contents

1. [Description](#description)
2. [Assumptions](#assumptions)
3. [Solution](#solution)
4. [Installation](#installation)
5. [Usage and testing](#usage-and-testing)
6. [What could be improved](#what-could-be-improved)
7. [Author](#author)

# Description

This is an technical assessment for [back-end developer](https://www.taotesting.com/careers/back-end-developer/) at [OAT](https://www.taotesting.com/).

I was given the instructions to implement an API to handle two endpoints following [Open API](https://www.openapis.org/) standards. All the details can be found in [ASSESSMENT.md](ASSESSMENT.md) file.

> Honestly, I'm not building API too often. I heard of Open API and had a quick look at it. Sadly never played with it for longer. I find this assessment interesting as I needed to google a bit (Swagger and Laravel integrations). 
 
# Assumptions

- As stated in the instructions, provided solution has to be created on top of widely-known framework. I'm using **Laravel 5**, as it's my every-day tool in work.
- In order to utilize Google Translate API I'm using the same library as recommended in the instructions   
- I don care if there is any Laravel package wrapping `stichoza/google-translate-php`. I want to create my own wrapper and enjoy this task more
- Although, it's not specified in instructions, I'll translate everything (both questions and answers)
    > Sometimes answer is not translatable or it makes no sense to translate (for example acronyms). I'll blindly trust that everything should be translated tho.
- To test the endpoints I'll use Postman (I'll also import `open-api.yaml` specification to it)

# Solution.

- [x] Implement two endpoints (`GET` and `POST` only) following given [specification](documentation/open-api.yaml)
- [x] Support `lang` parameter with `GET` endpoint
- [x] Questions can be imported from two types of source (`json` and `CSV`)
- [x] System is easily extendable (`ReaderContract` is the only interface necessary to implement with future reader)
- [x] No other data is imported from any other data source than provided files
- [x] Use `stichoza/google-translate-php`

# Installation

Given solution is dockerized and it's recommended way to install it.

> It's also possible to use different method, but it's not mentioned in this document. Use at your own risk!

To lunch application run following command in the terminal:

```bash
docker-compose up -d # Application is now running on 8080 port.
```

And before firing requests:

```bash
docker-compose exec php-fpm composer install # Install dependencies.
```

To customize it's behaviour you might want to change following environment variables:

- `QUESTIONS_PATH` - A relative (inside container) path to the questions source file
- `QUESTIONS_LANGUAGE` - Source file language (necessary for proper translation)

# Usage and testing

As stated under assumptions chapter, I'm using Postman to test the endpoints. Collection was exported [here (file)](documentation/Assessment%20API.postman_collection.json).

There are also some unit tests. To see them passing execute:

```bash
docker-compose exec php-fpm vendor/bin/phpunit
```

# What could be improved

- Remove boilerplate and scaffolded code that came together with Laravel (it's useless as this is only API)
    > Personally I hate looking and leftovers like that. I feel bad for leaving it, sorry. 
- Authorization and authentication (it's obvious this is necessary part of an API, but instructions were not requiring it)
    > One of the possibilities would be Laravel Passport and/or JWT.
- Documentation (for example C4 diagram)
- API versioning
- Remove `createdAt` parameter from `POST` endpoint as it's used to create new resources
    > Newly created questions should have current timestamp.
- Fix typo in `database/resources/questions.csv`
    > Second choice column is missing number (It should be "Choice 2").
- Caching `GET` endpoint and clearing cache after `POST`
    > This way we could save Google Translate API quota and generally speed it up.
- Create value-objects or use something like `jenssegers/model` to manage (de)serialization
    > This has quite high priority.
- Question uniqueness validation
    > This also has high priority.
- Simplify docker
    > Nginx isn't really necessary.
- Concrete Exceptions in other components
    >Similarly to `ReadException` and `ProcessException` in `Reader` component. Handle them correctly with `ExceptionHandler`.

# Author

Michał Haracewiat <m.haracewiat@gmail.com>
