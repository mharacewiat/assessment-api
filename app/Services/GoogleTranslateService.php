<?php

namespace App\Services;

use Stichoza\GoogleTranslate\GoogleTranslate as Client;

/**
 * GoogleTranslateService class.
 *
 * @package App\Services
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class GoogleTranslateService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * GoogleTranslateService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string      $string
     * @param string|null $language
     * @return string
     * @throws \ErrorException
     */
    public function translate(string $string, ?string $language): string
    {
        if (!$language) {
            return $this->client->translate($string);
        }

        return (clone $this->client)
            ->setTarget($language)
            ->translate($string);
    }
}
