<?php

namespace App\Providers;

use App\Services\GoogleTranslateService;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (!$this->app->environment('production')) {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Collection::macro('translate', function (string $language) {
            /** @var $this Collection */
            return $this->map(function ($value) use ($language) {
                if (is_iterable($value)) {
                    return collect($value)->translate($language);
                }

                return app(GoogleTranslateService::class)->translate($value, $language);
            });
        });

        Response::macro('translate', function (iterable $value, string $language) {
            return collect($value)->translate($language);
        });
    }
}
