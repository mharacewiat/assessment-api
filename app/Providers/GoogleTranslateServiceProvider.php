<?php

namespace App\Providers;

use App\Services\GoogleTranslateService;
use Illuminate\Support\ServiceProvider;
use Stichoza\GoogleTranslate\GoogleTranslate as Client;

/**
 * GoogleTranslateServiceProvider class.
 *
 * @package App\Providers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class GoogleTranslateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(GoogleTranslateServiceProvider::class, function () {
            return new GoogleTranslateService(
                (new Client())->setSource(config('questions.language'))
            );
        });
    }
}
