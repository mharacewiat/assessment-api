<?php

namespace App\Providers;

use App\Managers\QuestionsManagerContract;
use App\Managers\QuestionsManagerFactory;
use Illuminate\Support\ServiceProvider;

/**
 * QuestionsManagerServiceProvider class.
 *
 * @package App\Providers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class QuestionsManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(QuestionsManagerContract::class, function () {
            return QuestionsManagerFactory::create(config('questions.path'));
        });
    }
}
