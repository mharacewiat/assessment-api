<?php

namespace App\Writers;

use App\Readers\JsonReader;

/**
 * JsonWriter class.
 *
 * @package App\Writers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class JsonWriter extends Writer implements WriterContract
{
    /**
     * {@inheritDoc}
     */
    public function write(iterable $question): bool
    {
        $questions   = $this->readQuestions();
        $questions[] = $question;

        file_put_contents($this->path, json_encode($questions));

        return true;
    }

    /**
     * @return iterable
     * @throws \Exception
     */
    private function readQuestions(): iterable
    {
        return (new JsonReader($this->path))->read();
    }
}
