<?php

namespace App\Writers;

/**
 * CsvWriter class.
 *
 * @package App\Writers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class CsvWriter extends Writer implements WriterContract
{
    /**
     * {@inheritDoc}
     */
    public function write(iterable $question): bool
    {
        $handle = fopen($this->path, 'a+');
        fputcsv($handle, $question);

        return true;
    }
}
