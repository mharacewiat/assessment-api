<?php

namespace App\Writers;

/**
 * Writer class.
 *
 * @package App\Writers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
abstract class Writer implements WriterContract
{
    /**
     * @var string
     */
    protected $path;

    /**
     * Writer constructor.
     *
     * @param string $path
     */
    final public function __construct(string $path)
    {
        $this->setPath($path);
    }

    /**
     * @param string $path
     * @return WriterContract
     */
    private function setPath(string $path): WriterContract
    {
        $this->path = $path;
        return $this;
    }
}
