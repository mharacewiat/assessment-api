<?php

namespace App\Writers;

/**
 * WriterContract class.
 *
 * @package App\Writers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
interface WriterContract
{
    /**
     * Write whole content to given path.
     *
     * @param iterable $question
     * @return bool
     */
    public function write(iterable $question): bool;
}
