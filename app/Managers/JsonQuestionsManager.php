<?php

namespace App\Managers;

/**
 * JsonQuestionsManager class.
 *
 * @package App\Managers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class JsonQuestionsManager extends QuestionsManager
{
    /**
     * {@inheritDoc}
     */
    public function get(): iterable
    {
        return $this->reader->read();
    }

    /**
     * {@inheritDoc}
     */
    public function post(iterable $question): iterable
    {
        $this->writer->write($question);
        return $question;
    }
}
