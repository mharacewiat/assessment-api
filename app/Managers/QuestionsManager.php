<?php

namespace App\Managers;

use App\Readers\ReaderContract;
use App\Writers\WriterContract;

/**
 * QuestionsManager class.
 *
 * @package App\Managers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
abstract class QuestionsManager implements QuestionsManagerContract
{
    /**
     * @var ReaderContract
     */
    protected $reader;

    /**
     * @var WriterContract
     */
    protected $writer;

    /**
     * QuestionsManager constructor.
     *
     * @param ReaderContract $reader
     * @param WriterContract $writer
     */
    public function __construct(ReaderContract $reader, WriterContract $writer)
    {
        $this->reader = $reader;
        $this->writer = $writer;
    }
}
