<?php

namespace App\Managers;

/**
 * CsvQuestionsManager class.
 *
 * @package App\Managers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class CsvQuestionsManager extends QuestionsManager
{
    /**
     * {@inheritDoc}
     */
    public function get(): iterable
    {
        $questions = $this->decode($this->reader->read());
        return $questions;
    }

    /**
     * {@inheritDoc}
     */
    public function post(iterable $question): iterable
    {
        $this->writer->write($this->encode($question));
        return $question;
    }

    /**
     * @param iterable $questions
     * @return iterable
     */
    private function decode(iterable $questions): iterable
    {
        return collect($questions)
            ->map(function (iterable $question): iterable {
                return [
                    'text'      => $question['Question text'],
                    'createdAt' => $question['Created At'],
                    'choices'   => [
                        [
                            'text' => $question['Choice 1'],
                        ],
                        [
                            'text' => $question['Choice'],
                        ],
                        [
                            'text' => $question['Choice 3'],
                        ],
                    ],
                ];
            });
    }

    /**
     * @param iterable $question
     * @return iterable
     */
    private function encode(iterable $question): iterable
    {
        return [
            'Question text' => $question['text'],
            'Created At'    => $question['createdAt'],
            'Choice 1'      => $question['choices'][0]['text'],
            'Choice'        => $question['choices'][1]['text'],
            'Choice 3'      => $question['choices'][2]['text'],
        ];
    }
}
