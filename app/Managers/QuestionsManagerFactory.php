<?php

namespace App\Managers;

use App\Readers\CsvReader;
use App\Readers\JsonReader;
use App\Writers\CsvWriter;
use App\Writers\JsonWriter;

/**
 * QuestionsManager class.
 *
 * @package App\Managers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class QuestionsManagerFactory
{
    /**
     * @param string $path
     * @return QuestionsManagerContract
     * @throws \Exception
     */
    public static function create(string $path): QuestionsManagerContract
    {
        switch ($extension = pathinfo($path, PATHINFO_EXTENSION)) {
            case 'csv':
                return new CsvQuestionsManager(
                    new CsvReader($path),
                    new CsvWriter($path)
                );
            case 'json':
                return new JsonQuestionsManager(
                    new JsonReader($path),
                    new JsonWriter($path)
                );
            default:
                /* Do nothing. */
                break;
        }

        throw new \Exception(sprintf('Unsupported file extension (%s)', $extension));
    }
}
