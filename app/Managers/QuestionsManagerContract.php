<?php

namespace App\Managers;

/**
 * QuestionsManagerContract class.
 *
 * @package App\Managers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
interface QuestionsManagerContract
{
    /**
     * @return iterable
     */
    public function get(): iterable;

    /**
     * @param iterable $question
     * @return iterable
     */
    public function post(iterable $question): iterable;
}
