<?php

namespace App\Readers;

/**
 * Reader class.
 *
 * @package App\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
abstract class Reader implements ReaderContract
{
    /**
     * @var string
     */
    protected $path;

    /**
     * Reader constructor.
     *
     * @param string $path
     */
    final public function __construct(string $path)
    {
        $this->setPath($path);
    }

    /**
     * @param string $path
     * @return ReaderContract
     */
    private function setPath(string $path): ReaderContract
    {
        $this->path = $path;
        return $this;
    }
}
