<?php

namespace App\Readers;

/**
 * ReaderContract class.
 *
 * @package App\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
interface ReaderContract
{
    /**
     * Read data from given path and return collective data.
     *
     * @return iterable
     */
    public function read(): iterable;
}
