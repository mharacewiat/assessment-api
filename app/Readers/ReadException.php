<?php

namespace App\Readers;

/**
 * ReadException class.
 *
 * @package App\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class ReadException extends \Exception
{
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct('Unable to read CSV file', $previous->getCode(), $previous);
    }
}
