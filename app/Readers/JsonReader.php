<?php

namespace App\Readers;

/**
 * JsonReader class.
 *
 * @package App\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class JsonReader extends Reader implements ReaderContract
{
    /**
     * {@inheritDoc}
     */
    public function read(): iterable
    {
        try {
            return $this->decode();
        } catch (\Exception $exception) {
            throw new \Exception('Decode failure', $exception->getCode(), $exception);
        }

        throw new \Exception('Read failure');
    }

    /**
     * @return iterable
     * @throws \Exception
     */
    private function decode(): iterable
    {
        $questions = json_decode(file_get_contents($this->path), true);

        if (is_null($questions) || json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Invalid format');
        }

        return $questions;
    }
}
