<?php

namespace App\Readers;

/**
 * CsvReader class.
 *
 * @todo    Consider supporting delimiter, enclosure and escape customization {@see https://www.php.net/manual/en/function.fgetcsv.php}.
 *
 * @package App\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class CsvReader extends Reader implements ReaderContract
{
    /**
     * {@inheritDoc}
     */
    public function read(): iterable
    {
        try {
            return $this->process();
        } catch (ProcessException $exception) {
            /* Do nothing. */
        }

        throw new ReadException();
    }

    /**
     * @return iterable
     * @throws ProcessException
     */
    private function process(): iterable
    {
        $questions = [];

        try {
            $handle = fopen($this->path, 'r');
            $header = fgetcsv($handle);
        } catch (\Throwable $exception) {
            throw new ProcessException($exception);
        }

        while ($question = fgetcsv($handle)) {
            $questions[] = array_combine($header, $question);
        }

        fclose($handle);

        return $questions;
    }
}
