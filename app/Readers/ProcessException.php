<?php

namespace App\Readers;

/**
 * ProcessException class.
 *
 * @package App\Readers
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class ProcessException extends \Exception
{
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct('Process failure', $previous->getCode(), $previous);
    }
}
