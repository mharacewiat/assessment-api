<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * GetQuestionRequest class.
 *
 * @property-read string $lang A two-letter language code {@see https://en.wikipedia.org/wiki/ISO_639-1}
 *
 * @package App\Http\Requests
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class GetQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lang' => [
                'required',
                'size:2',
            ],
        ];
    }
}
