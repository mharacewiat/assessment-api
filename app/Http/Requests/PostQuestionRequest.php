<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * PostQuestionRequest class.
 *
 * @package App\Http\Requests
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class PostQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'           => 'required',
            'createdAt'      => [
                'required',
                'date_format:Y-m-d H:i:s',
            ],
            'choices'        => [
                'required',
                'array',
                'size:3',
            ],
            'choices.*'      => [
                'required',
                'array',
            ],
            'choices.*.text' => [
                'required',
            ],
        ];
    }
}
