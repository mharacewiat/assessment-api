<?php

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostQuestionRequest;
use App\Managers\QuestionsManagerContract;

/**
 * PostQuestionsController class.
 *
 * @package App\Http\Controllers\Questions
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class PostQuestionsController extends Controller
{
    public function __invoke(PostQuestionRequest $request, QuestionsManagerContract $manager)
    {
        return $manager->post($request->all());
    }
}
