<?php

namespace App\Http\Controllers\Questions;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetQuestionRequest;
use App\Managers\QuestionsManagerContract;

/**
 * GetQuestionsController class.
 *
 * @package App\Http\Controllers\Questions
 * @author  Michał Haracewiat <admin@haracewiat.pl>
 */
class GetQuestionsController extends Controller
{
    public function __invoke(GetQuestionRequest $request, QuestionsManagerContract $manager)
    {
        return response()->translate(
            $manager->get(),
            $request->lang
        );
    }
}
